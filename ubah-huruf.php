<?php
function ubah_huruf($string){
    
    for($i=0; $i<strlen($string); $i++){
        switch ($string[$i]) {
            case 'a':
                $string[$i]='b';
                break;
            case 'b':
                $string[$i]='c';
                break;
            case 'c':
                $string[$i]='d';
                break;
            case 'd':
                $string[$i]='e';
                break;
            case 'e':
                $string[$i]='f';
                break;
            case 'f':
                $string[$i]='g';
            break;
            case 'g':
                $string[$i]='h';
                break;
            case 'h':
                $string[$i]='i';
            break;
            case 'i':
                $string[$i]='j';
                break;
            case 'j':
                $string[$i]='k';
            break;
            case 'k':
                $string[$i]='l';
                break;
            case 'l':
                $string[$i]='m';
            break;
            case 'm':
                $string[$i]='n';
                break;
            case 'n':
                $string[$i]='o';
            break;
            case 'o':
                $string[$i]='p';
                break;
            case 'p':
                $string[$i]='q';
            break;
            case 'q':
                $string[$i]='r';
                break;
            case 'r':
                $string[$i]='s';
            break;
            case 's':
                $string[$i]='t';
                break;
            case 't':
                $string[$i]='u';
            break;
            case 'u':
                $string[$i]='v';
                break;
            case 'v':
                $string[$i]='w';
            break;
            case 'w':
                $string[$i]='x';
                break;
            case 'x':
                $string[$i]='y';
            break;
            case 'y':
                $string[$i]='z';
                break;
            case 'z':
                $string[$i]='a';
            break;

        }
        
    }
    return $string . '<br>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>