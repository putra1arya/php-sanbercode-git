<?php
function tentukan_nilai($number)
{
    //number >= 85 dan <= 100 maka sangat baik
    //number >= 70 dan < 85 maka baik
    //number >= 60 dan < 70 maka cukup
    //else kurang
    if($number>=85 && $number<= 100){
        return 'Sangat Baik <br>';
    }else if( $number>=70 && $number< 85 ){
        return 'Baik <br>';
    }else if( $number>=60 && $number< 70){
        return 'Cukup <br>';
    }else{
        return 'Kurang <br>';
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>